Reference
=========

Purpose
-------

I created these modules because I often run into times where I need to define a word, or find synonyms quickly, and opening a browser takes too long.

TODO
-----

Create Synonym file, find more dictionaries


Define
-------

This file finds definitions for words

Synonym
--------

- name is a WIP

This file finds synonyms for words

Websites Used
-------------

- reference.com
- merriam-webster.com
-


